/*
 * Copyright 2023 jacqueline <me@jacqueline.id.au>
 *
 * SPDX-License-Identifier: GPL-3.0-only
 */

#include "drivers/nanobuttons.hpp"
#include <stdint.h>

#include <cstdint>

#include "assert.h"
#include "driver/gpio.h"
#include "driver/i2c.h"
#include "esp_err.h"
#include "esp_log.h"
#include "freertos/projdefs.h"
#include "hal/gpio_types.h"
#include "hal/i2c_types.h"

#include "drivers/i2c.hpp"

namespace drivers {

[[maybe_unused]] static const char* kTag = "NANOBUTTONS";
static const uint8_t kNanoButtonsAddress = 0x1C;
static const gpio_num_t kIntPin = GPIO_NUM_25;

NanoButtons::NanoButtons() {
  gpio_config_t int_config{
      .pin_bit_mask = 1ULL << kIntPin,
      .mode = GPIO_MODE_INPUT,
      .pull_up_en = GPIO_PULLUP_ENABLE,
      .pull_down_en = GPIO_PULLDOWN_DISABLE,
      .intr_type = GPIO_INTR_DISABLE,
  };
  gpio_config(&int_config);
}

NanoButtons::~NanoButtons() {}

void NanoButtons::WriteRegister(uint8_t reg, uint8_t val) {
  I2CTransaction transaction;
  transaction.start()
      .write_addr(kNanoButtonsAddress, I2C_MASTER_WRITE)
      .write_ack(reg)
      .stop();
  esp_err_t res = transaction.Execute(1);
  if (res != ESP_OK) {
    ESP_LOGW(kTag, "write failed: %s", esp_err_to_name(res));
  }
}

uint8_t NanoButtons::ReadRegister(uint8_t reg) {
  uint8_t res;
  I2CTransaction transaction;
  transaction.start()
      .write_addr(kNanoButtonsAddress, I2C_MASTER_WRITE)
      .write_ack(reg)
      .start()
      .write_addr(kNanoButtonsAddress, I2C_MASTER_READ)
      .read(&res, I2C_MASTER_NACK)
      .stop();
  if (transaction.Execute(1) == ESP_OK) {
    return res;
  } else {
    return 0;
  }
}

void NanoButtons::Update() {
  bool has_data = !gpio_get_level(kIntPin);
  if (!has_data) {
    return;
  }
  
  data_.is_wheel_touched = false;
  data_.is_button_touched = false;

  uint8_t dir = ReadRegister(2);
  switch (dir) {
    case 0x01:
      data_.wheel_position = 0;
      data_.is_wheel_touched = true;
      break;
    case 0x02:
      data_.wheel_position = 192;
      data_.is_wheel_touched = true;
      break;
    case 0x04:
      data_.wheel_position = 128;
      data_.is_wheel_touched = true;
      break;
    case 0x08:
      data_.wheel_position = 64;
      data_.is_wheel_touched = true;
      break;
    case 0x10:
      data_.is_button_touched = true;
      break;
    case 0x00:
      break;
    default:
      ESP_LOGI(kTag, "unknown_combo_pressed: %u", dir);
      // Ignore multiple button presses
      break;
  }
    
  if (data_.is_wheel_touched)
  {
    ESP_LOGI(kTag, "direction_pressed: %u", data_.wheel_position);
  }
}

TouchWheelData NanoButtons::GetTouchWheelData() const {
  return data_;
}

auto NanoButtons::Recalibrate() -> void {
}

auto NanoButtons::LowPowerMode(bool lowpower) -> void {
  WriteRegister(LOW_POWER, lowpower ? 1 : 0);
}

}  // namespace drivers
