/*
 * Copyright 2023 jacqueline <me@jacqueline.id.au>
 *
 * SPDX-License-Identifier: GPL-3.0-only
 */

#pragma once

#include <stdint.h>
#include <functional>

#include "esp_err.h"
#include "result.hpp"

#include "drivers/gpios.hpp"
#include "touchwheel.hpp"

namespace drivers {

class NanoButtons : public TouchWheel {
 public:
  static auto Create() -> NanoButtons* { return new NanoButtons(); }
  NanoButtons();
  ~NanoButtons();

  // Not copyable or movable.
  NanoButtons(const NanoButtons&) = delete;
  NanoButtons& operator=(const NanoButtons&) = delete;

  auto Update() -> void override;
  auto GetTouchWheelData() const -> TouchWheelData override;

  auto Recalibrate() -> void override;
  auto LowPowerMode(bool en) -> void override;

 private:
  TouchWheelData data_;

  enum Register {
    FIRMWARE_VERSION = 1,
    READ_BUTTONS = 2,
    LOW_POWER = 8,
  };

  void WriteRegister(uint8_t reg, uint8_t val);
  uint8_t ReadRegister(uint8_t reg);
};

}  // namespace drivers
